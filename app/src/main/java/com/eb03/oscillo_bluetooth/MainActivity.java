package com.eb03.oscillo_bluetooth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.eb03.RotaryButton.RotaryButton;
import com.eb03.dimmer.R;
import com.eb03.oscillo_bluetooth.bluetooth.BTConnectActivity;
import com.eb03.oscillo_bluetooth.bluetooth.Transceiver;



import static android.os.Build.VERSION.SDK_INT;

public class MainActivity extends AppCompatActivity {

    private final static int BT_CONNECT_CODE = 1;
    private final static int PERMISSIONS_REQUEST_CODE = 0;
    private final static String[] BT_DANGEROUS_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    private TextView mStatus;
    private OscilloManager mOscilloManager;
    private RotaryButton mRB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mStatus = findViewById(R.id.status);

        mRB = findViewById(R.id.rotaryButton);
        mRB.setEnabled(false);

        verifyBtRights();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mOscilloManager = OscilloManager.getInstance();
        mOscilloManager.setOscilloEventsListener(new OscilloManager.OscilloEventsListener() {
            @Override
            public void onDeviceDataSend(byte[] data) {

            }

            @Override
            public void onDeviceStateChanged(int state) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        switch (state){
                            case Transceiver.STATE_CONNECTED:
                                Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show();
                                mRB.setEnabled(true);
                                break;
                            case Transceiver.STATE_CONNECTING:
                                Toast.makeText(MainActivity.this, "Connecting", Toast.LENGTH_SHORT).show();
                                break;
                            case Transceiver.STATE_NOT_CONNECTED:
                                Toast.makeText(MainActivity.this, "Not connected", Toast.LENGTH_SHORT).show();
                                mRB.setEnabled(false);
                                break;
                            default:
                        }
                        invalidateOptionsMenu();
                    }
                });
            }
        });

        mRB.setRotaryButtonChangeListener(new RotaryButton.RotaryButtonChangeListener() {
            @Override
            public void onChange(float v) {
                mOscilloManager.setCalibrationDutyCycle(v);
            }

            @Override
            public void onDoubleClick(float v) {
                mOscilloManager.setCalibrationDutyCycle(v);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        switch (mOscilloManager.getStatus()){
            case Transceiver.STATE_NOT_CONNECTED:
                menu.findItem(R.id.connect).setVisible(true);
                menu.findItem(R.id.disconnect).setVisible(false);
                break;
            case Transceiver.STATE_CONNECTED:
                menu.findItem(R.id.disconnect).setVisible(true);
                menu.findItem(R.id.connect).setVisible(false);
                break;
            default:
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int menuItem = item.getItemId();
        switch(menuItem){
            case R.id.connect:
                Intent BTConnect;
                BTConnect = new Intent(this, BTConnectActivity.class);
                startActivityForResult(BTConnect,BT_CONNECT_CODE);
        }
        return true;
    }

    private void verifyBtRights(){
        if(BluetoothAdapter.getDefaultAdapter() == null){
            Toast.makeText(this,"Cette application nécessite un adaptateur BT",Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        if(SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_DENIED){
                requestPermissions(BT_DANGEROUS_PERMISSIONS,PERMISSIONS_REQUEST_CODE);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSIONS_REQUEST_CODE){
            if(grantResults[0] == PackageManager.PERMISSION_DENIED){
                Toast.makeText(this,"Les autorisations BT sont requises pour utiliser l'application",Toast.LENGTH_LONG).show();
                finish();
                return;
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BT_CONNECT_CODE:
                if (resultCode == RESULT_OK) {
                    String address = data.getStringExtra("device");
                    mStatus.setText(address);
                    mOscilloManager.connect(address);

                }
                break;
            default:
        }


    }
}



