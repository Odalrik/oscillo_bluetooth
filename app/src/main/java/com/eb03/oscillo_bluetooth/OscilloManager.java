package com.eb03.oscillo_bluetooth;

import com.eb03.oscillo_bluetooth.bluetooth.BTManager;
import com.eb03.oscillo_bluetooth.bluetooth.Transceiver;
import com.eb03.oscillo_bluetooth.frames.FrameProcessor;

/**
 * Singleton servant d'interface entre le Transceiver et l'UI.
 */
public class OscilloManager  {

    /* ******************************************************
     * ********************* VARIABLES **********************
     * *****************************************************/

    private static OscilloManager INSTANCE;

    private Transceiver mOscillo;

    private FrameProcessor mFrameProcessor;
    private OscilloEventsListener mOscilloEventsListener;
    private Transceiver.TransceiverListener mTransceiverListener;

    private final byte COMMAND = 0x0A;

    private byte mDutyCycle; //rapport cyclique du signal de calibration


    /* ******************************************************
     * ******************* CONSTRUCTEUR *********************
     * *****************************************************/

    public OscilloManager() {
        attachTransceiver(new BTManager());

        mOscillo.attachFrameProcessor(new FrameProcessor());
    }


    /* ******************************************************
     * ********************** METHODES **********************
     * *****************************************************/
    /**
     * Attache un Transceiver au gestionnaire.
     * @param transceiver Transceiver à attacher.
     */
    public void attachTransceiver(Transceiver transceiver){
        mOscillo = transceiver;
        mOscillo.setTransceiverListener(mTransceiverListener);
    }

    /**
     * Détache le Transceiver.
     */
    public void detachTransceiver(){
        mOscillo = null;
    }

    /**
     * Connecte le Transceiver.
     * @param id Adresse de l'appareil.
     */
    public void connect(String id) {
        mOscillo.connect(id);
    }

    /* ******************************************************
     * ********************* SINGLETON **********************
     * *****************************************************/
    /**
     * @return Renvoie l'instance du singleton.
     */
    public static OscilloManager getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new OscilloManager();
        }

        return INSTANCE;
    }


    /* ******************************************************
     * ****************** GETTERS/SETTERS *******************
     * *****************************************************/

    /**
     * @param oscilloEventsListener Event Listener de de l'oscillo
     */
    public void setOscilloEventsListener(OscilloEventsListener oscilloEventsListener) {
        this.mOscilloEventsListener = oscilloEventsListener;
    }

    /**
     * @param alpha Le Ratio du temps de cycle, récupéré du bouton rotatif
     */
    public void setCalibrationDutyCycle(float alpha) {
        this.mDutyCycle = (byte) (alpha*100);
        byte[] sentCommand = {COMMAND, };
        mOscillo.send(sentCommand);
    }

    public int getStatus(){
        if(mOscillo != null) return mOscillo.getState();
        else return Transceiver.STATE_NOT_CONNECTED;
    }

    /* ******************************************************
     * ********************* INTERFACE **********************
     * *****************************************************/

    public interface OscilloEventsListener{
        /**
         * Est appelée lorsque des données sont envoyées.
         * @param data données envoyées.
         */
        void onDeviceDataSend(byte[] data);


        /**
         * Est appelée lors d'un changement d'état du Transceiver.
         * @param state État du Transceiver.
         */
        void onDeviceStateChanged(int state);
    }
}
