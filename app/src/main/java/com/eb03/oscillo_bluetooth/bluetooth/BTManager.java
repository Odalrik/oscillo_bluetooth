package com.eb03.oscillo_bluetooth.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

//import com.eb03.RotaryButton.RotaryButton;
import com.eb03.oscillo_bluetooth.frames.ByteRingBuffer;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.BufferOverflowException;
import java.util.UUID;

/**
 * Classe de gestion de la connexion Bluetooth.
 */
public class BTManager extends Transceiver {

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private BluetoothAdapter mAdapter;

    private BluetoothSocket mSocket = null;

    private ConnectThread mConnectThread = null;
    private WritingThread mWritingThread = null;

    private ByteRingBuffer buffer = null;

    /**
     * Création du gestionnaire Bluetooth.
     */
    public BTManager() {
        buffer = new ByteRingBuffer(512);
        mAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public void connect(String id) {
        BluetoothDevice device = mAdapter.getRemoteDevice(id);
        disconnect();
        mConnectThread = new ConnectThread(device);
        setState(STATE_CONNECTING);
        mConnectThread.start();
    }


    @Override
    public void disconnect() {
        try {
            mSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mSocket = null;
        setState(Transceiver.STATE_NOT_CONNECTED);
    }


    @Override
    public void send(byte[] b) {
        try {
            buffer.put(toFrame(b));
        } catch (BufferOverflowException e) {

        }
    }

    /**
     * Classe du Thread de connexion au device
     */
    private class ConnectThread extends Thread{


        public ConnectThread(BluetoothDevice device) {
            try {
                mSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            mAdapter.cancelDiscovery();

            try {
                mSocket.connect();
            } catch (IOException e) {
                disconnect();
                setState(Transceiver.STATE_NOT_CONNECTED);
            }

            setState(Transceiver.STATE_CONNECTED);
            startReadWriteThreads();

        }
    }


    private void startReadWriteThreads(){
        // instanciation d'un thread de lecture

        mWritingThread = new WritingThread(mSocket);
        Log.i("ConnectThread","Thread WritingThread lancé");
        mWritingThread.start();
        setState(STATE_CONNECTED);
    }

    /**
     * Classe du Thread d'écriture vers le device
     */
    private class WritingThread extends Thread{
        private OutputStream mOutStream;
        private ByteRingBuffer mRingBuffer;

        public WritingThread(BluetoothSocket mSocket) {
            try {
                mOutStream = mSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            while(mSocket != null){
                try {
                    if(!buffer.isEmpty()) mOutStream.write(buffer.getAll());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void write(byte[] data){
            mRingBuffer.put(data);
        }

    }
}
