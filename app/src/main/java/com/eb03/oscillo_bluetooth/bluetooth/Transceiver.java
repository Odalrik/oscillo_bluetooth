package com.eb03.oscillo_bluetooth.bluetooth;

import com.eb03.oscillo_bluetooth.frames.FrameProcessor;

/**
 * Classe abstraite représentant une interface de communication.
 */
public abstract class Transceiver {

    public static final int STATE_NOT_CONNECTED = 0; // non connecté
    public static final int STATE_CONNECTING = 1;    // connexion en cours
    public static final int STATE_CONNECTED = 2;     // connecté


   private int mState;
   protected TransceiverListener mTransceiverListener;
   private FrameProcessor mFrameProcessor;

    /**
     * @param transceiverListener EventListener de la classe.
     */
   public void setTransceiverListener(TransceiverListener transceiverListener){
       mTransceiverListener = transceiverListener;
   }

    /**
     * Attache un gestionnaire de trames.
     * @param frameProcessor FrameProcessor pour encoder et décoder les données.
     */
   public void attachFrameProcessor(FrameProcessor frameProcessor){
       mFrameProcessor = frameProcessor;
   }

    /**
     * Détache le gestionnaire de trames.
     */
    public void detachFrameProcessor(){
       mFrameProcessor = null;
    }

    /**
     * Permet de construire les trames à envoyer à l'oscilloscope
     * @param data Data à mettre en trame
     * @return trame
     */
    public byte[] toFrame(byte[] data) {return mFrameProcessor.toFrame(data);}

    /**
     * @return état du Transceiver.
     */
    public int getState() {
        return mState;
    }

    /**
     *
     * @param state état du Transceiver.
     */
    public void setState(int state) {
        mState = state;
        if(mTransceiverListener != null){
            mTransceiverListener.onTransceiverStateChanged(state);
        }
    }


    /* ******************************************************
     * ********************* INTERFACE **********************
     * *****************************************************/

    public interface TransceiverListener{

        /**
         * Est appelée lors d'un changement d'état du Transceiver.
         * @param state État du Transceiver.
         */
        void onTransceiverStateChanged(int state);
        /**
         * Est appelée lorsque la connection est perdue.
         */
        void onTransceiverConnectionLost();

        /**
         * Est appelée lorsque la connection n'a pu être établie.
         */
        void onTransceiverUnableToConnect();
   }


    /* ******************************************************
     * **************** METHODES ABSTRAITES *****************
     * *****************************************************/
    /**
     * Connexion à l'appareil.
     * @param id adresse de l'appareil.
     */
    public abstract void connect(String id);

    /**
     * Déconnexion de l'appareil.
     */
    public abstract void disconnect();

    /**
     * Envoie de données vers l'appareil.
     * @param b données à envoyer.
     */
    public abstract void send(byte[] b);


}
