package com.eb03.oscillo_bluetooth.frames;


/**
 * Valeurs des acknowledgement pour les trames sortantes
 */
public enum ACK {

    CORRECT(0x0),
    INCORRECT(0x1),
    DONE(0x2),
    ERROR(0x3);

    public final byte CODE;

    /**
     * @param CODE Code de l'ACK.
     */
    ACK(int CODE) {
        this.CODE = (byte) CODE;
    }

    public static ACK getACK(int code) {
        for(ACK e: ACK.values()) {
            if(e.CODE == code) {
                return e;
            }
        }
        return null;// not found
    }

}