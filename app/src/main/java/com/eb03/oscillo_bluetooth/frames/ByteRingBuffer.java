package com.eb03.oscillo_bluetooth.frames;

import androidx.annotation.NonNull;

import java.util.Arrays;

/**
 * Classe de buffer circulaire de bytes.
 */
public class ByteRingBuffer {
    /* ******************************************************
     * ************** VARIABLES DE LA CLASSE ****************
     * *****************************************************/
    private int mBufferSize;    //Taille totale du buffer

    private int mWriteIndex;    //Index de la valeur à écrire
    private int mReadIndex;     //Index de la valeur à lire
    private int mOccupiedSpace; //Taille occupée du buffer

    private byte[] mBuffer;     //Buffer


    /* ******************************************************
     * ************* CONSTRUCTEUR DE LA CLASSE **************
     * *****************************************************/

    /**
     * Constructeur de la classe ByteRingBuffer
     *
     * @param bufferSize Taille du buffer
     */
    public ByteRingBuffer(int bufferSize) {
        this.mBufferSize = bufferSize;
        this.mBuffer = new byte[bufferSize];
        this.mOccupiedSpace = 0;
        this.mWriteIndex = 0;
        this.mReadIndex = 0;
    }


    /* ******************************************************
     * *************** METHODES DE LA CLASSE ****************
     * *****************************************************/

    /**
     * Lit un byte du buffer.
     * @return Byte lu.
     */
    public byte get() {
        byte retour = 0;

        if (!isEmpty()) {
            retour = mBuffer[mReadIndex];
            mReadIndex++;
            mOccupiedSpace--;
        }

        return retour;
    }

    /**
     * Lit tous les bytes du buffer.
     * @return Bytes du buffer.
     */
    public byte[] getAll() {
        int nBytes = mOccupiedSpace;
        byte[] bytesToRead = new byte[mOccupiedSpace];

        for (int i = 0; i < mOccupiedSpace; i++) {
            bytesToRead[i] = get();
        }

        return bytesToRead;
    }

    /**
     * Ecrit un byte dans le buffer.
     * @param data byte à écrire.
     * @return true si le buffer est en overflow.
     */
    public boolean put(byte data) {
        // On vérifie que le buffer n'est pas plein
        if (isOverflowed()) return true;

        // On ajoute la nouvelle valeur à l'emplacement d'écriture.
        mBuffer[mWriteIndex++] = data;

        if(mWriteIndex == mBufferSize) mWriteIndex = 0;
        mOccupiedSpace++;
        if(mOccupiedSpace > mBufferSize) mOccupiedSpace = mBufferSize;

        return false;
    }

    /**
     * Ecrit une suite de bytes dans le buffer.
     * @param data Bytes à écrire.
     * @return True si le buffer est en overflow.
     */
    public boolean put(byte[] data) {
        for(byte d: data) {
            if(put(d)) return true;
        }

        return false;
    }

    /**
     * Vérifie si le buffer est vide.
     * @return True si le buffer est vide.
     */
    public boolean isEmpty() {
        return (mOccupiedSpace == 0);
    }

    /**
     * Vérifie si le buffer est en overflow.
     * @return True si le buffer est en overflow.
     */
    public boolean isOverflowed() {
        return mOccupiedSpace == mBuffer.length;
    }

    @NonNull
    @Override
    public String toString() {
        return "ByteRingBuffer :" +
                "\n\tPointeur d'écriture = " + mWriteIndex +
                "\n\tPointeur de lecture = " + mReadIndex +
                "\n\tEspace occupé = " + mOccupiedSpace +
                "\n\tDonnées du buffer : " + Arrays.toString(mBuffer);
    }
}
