package com.eb03.oscillo_bluetooth.frames;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/**
 * Classe d'encodage et décodage des trames de communication.
 */
public class FrameProcessor {
    public static int HEADER = 0x05;
    public static int ESC = 0x06;
    public static int TAIL = 0x04;

    /**
     * Encode une suite de bytes en trame
     * @param data suite de bytes à encoder
     * @return trame encodée
     */
    public byte[] toFrame(byte[] data) {
        //data contient l'id commande + les arguments

        ByteArrayOutputStream frame = new ByteArrayOutputStream();
        byte control = 0;

        //HEADER
        frame.write(HEADER);

        //LENGTH
        byte[] length = new byte[2];

        length[1] = (byte) (((data.length) >> 8) & 0xFF);
        length[0] = (byte) ((data.length) & 0xFF);

        for(int n = 1 ; n >= 0 ; n--) {
            writeToFrame(frame,length[n]);
            control += length[n];
        }

        //PAYLOAD
        for(byte d:data) {
            writeToFrame(frame, d);
            control += d;
        }

        //CTRL
        control = twosComplement(control);
        frame.write(control);

        //TAIL
        frame.write(TAIL);

        return frame.toByteArray();
    }

    /**
     * Fonction permettant d'écrire dans un ByteArrayOutputStream en gérant les valeurs interdites.
     * Si on désire écrire une valeur interdite, on ajoute la valeur de l'offset suivie
     * de la somme de l'offset et de la valeur.
     * @param frame trame de destination.
     * @param data msg à écrire.
     */
    public void writeToFrame(ByteArrayOutputStream frame, byte data) {
        if(data == HEADER || data == TAIL || data == ESC)
        {
            frame.write(ESC);
            frame.write(data + ESC);
        }
        else frame.write(data);
    }

    /**
     * Fonction permettant de gérer le complément à 2 tel que demandé, avec un modulo 256.
     * @param data La valeur à traiter
     * @return La valeur traitée
     */
    public byte twosComplement(byte data) {
        return (byte) ((-data)%256);
    }
}
